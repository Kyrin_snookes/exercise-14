﻿using System;

namespace exercise_14
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            var month = "";
            System.Console.WriteLine("What month is your birthday");
            month = Console.ReadLine();
            System.Console.WriteLine($"your birthday is in {month}");

        
        
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
